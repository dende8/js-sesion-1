// var mcs = ["Bnet", "Gazir", "Mnak", "SweetPain"];

// var newLength = mcs.push("Blon", "Tirpa");

// console.log("He añadido 2 elementos nuevos y mi nueva longitud es", newLength);

// mcs.sort();

// mcs.reverse();

// console.log(mcs);

var edad = 68;

var soyMenor = edad < 18;

var tiempoTrabajado = 32;

var sueldo = 2000;

var mePuedoJubilar = edad > 65;
/**

* ¿Usamos la comparación dentro del if o la extraemos a una variable?

*

* Ejercicio: Si tengo mas de 65 años, he trabajado mas de 35 con un sueldo de 2000 y no soy menor de edad

* la variable mePuedoJubilar será true.

*/
var mePuedoJubilar = edad>=65 || (tiempoTrabajado>35 && sueldo >= 2000 && !soyMenor);

if (mePuedoJubilar){
    console.log("Te puedes jubilar");
}
else{
    console.log("No te puedes jubilar");
}